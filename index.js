// console.log("Hello World!")
const getCube = 4 ** 3
console.log(`The cube of 4 is ${getCube}`)

const address = ["Montalban Heights", "Rodriguez","Rizal 1870"]
const [villageName, municipality, provinceZip] = address

console.log(`I live at ${villageName} ${municipality}, ${provinceZip}`)

const animal = {
	type: " Great ape",
	weight: "300 pounds",
	height:	"6 feet"
}

function animalDesc ({type, weight, height}) {
	console.log(`Gorillas is a type of ${type}. Adult male weights ${weight} and stand at ${height} tall.`)
};

animalDesc(animal);

const numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) => console.log(`${number}`))

class Dog {
	constructor(name, age, breed) {
		this.name = name
		this.age = age
		this.breed = breed
	};
};

const myDog = new Dog("Donkey", "10 months", "Belgian Malinois");
console.log(myDog);

/*myDog.name = "Donkey"
myDog.age = "10 months"
myDog.breed = "Belgian Malinois"*/